import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export class DateValidators {
    static greaterThan(startControl: Date): ValidatorFn {
        return (endControl: AbstractControl): ValidationErrors | null => {
            const startDate: Date = startControl;
            const endDate: Date = new Date(endControl.value);
            if (!startDate || !endDate) {
                return null;
            }
            if (endDate > startDate) {
                return { greaterThan: true };
            }
            return null;
        };
    }
}