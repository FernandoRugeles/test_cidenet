import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditEmployeecomponent } from './components/edit-employee-component/edit.employee.component';
import { ListEmployessComponent } from './components/list-employess-component/list-employess.component';
import { RegisterEmployeeComponent } from './components/register-employee-component/register-employee.component';

const routes: Routes = [
  {
    path: '',
    component: ListEmployessComponent
  },
  {
    path: 'edit',
    component: EditEmployeecomponent
  },
  {
    path: 'register',
    component: RegisterEmployeeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListEmployessRouterModule { }
