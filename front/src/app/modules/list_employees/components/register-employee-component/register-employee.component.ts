import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { DateValidators } from 'src/app/validator/DateValidators';
import { IdentificationTypeEnum } from 'src/app/enums/IdentificationType';
import { AreaEnum } from 'src/app/enums/AreaEnum';
import { CountryEnum } from 'src/app/enums/CountryEnum';
import { EmployeeService } from 'src/app/services/employee.service';

interface EmployeeI {
  id: number,
  primerApellido: string,
  segundoApellido: string,
  primerNombre: string,
  otrosNombres: string,
  paisEmpleo: string,
  tipoIdentificacion: string,
  numeroIdentificacion: string,
  correElectronico: string,
  fechaIngreso: string,
  area: string,
  estado: string,
  fechaHoraRegistro: string
}

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.scss']
})
export class RegisterEmployeeComponent implements OnInit {
  public employees: EmployeeI[] = [];
  public employeeForm: FormGroup;
  public identificationTypesEnum = Object.values(IdentificationTypeEnum);
  public areaEnum = Object.values(AreaEnum);
  public countryEnum = Object.values(CountryEnum);

  constructor(
    private fb: FormBuilder,
    private employeesService: EmployeeService,
    private router: Router
  ) {
    this.employeeForm = this.fb.group({
      primerApellido: new FormControl('', [Validators.required, Validators.maxLength(20)]),
      segundoApellido: new FormControl('', [Validators.required, Validators.maxLength(20)]),
      primerNombre: new FormControl('', [Validators.required, Validators.maxLength(20)]),
      otrosNombres: new FormControl('', [Validators.maxLength(50)]),
      paisEmpleo: new FormControl('', [Validators.maxLength(20)]),
      tipoIdentificacion: new FormControl('', [Validators.maxLength(20)]),
      numeroIdentificacion: new FormControl('', [Validators.maxLength(20)]),
      //correElectronico: new FormControl('', [Validators.email, Validators.maxLength(45)]),
      fechaIngreso: new FormControl(new Date(), [DateValidators.greaterThan(new Date())]),
      area: new FormControl(''),
      estado: new FormControl(1),
      fechaHoraRegistro: new FormControl(new Date())
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.employeeForm.markAllAsTouched;
    this.employeeForm.markAsDirty;
    if(this.employeeForm.valid){
      this.employeesService.registerEmployee(this.employeeForm.value);
    }
  }

  goBack(){
    this.router.navigate(['/']);
  }
}
