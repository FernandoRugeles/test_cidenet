import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeService } from 'src/app/services/employee.service';


interface EmployeeI {
  id: number,
  primerApellido: string,
  segundoApellido: string,
  primerNombre: string,
  otrosNombres: string,
  paisEmpleo: string,
  tipoIdentificacion: string,
  numeroIdentificacion: string,
  correElectronico: string,
  fechaIngreso: string,
  area: string,
  estado: string,
  fechaHoraRegistro: string
}

@Component({
  selector: 'app-list-employess-component',
  templateUrl: './list-employess.component.html',
  styleUrls: ['./list-employess.component.scss']
})
export class ListEmployessComponent implements OnInit {

  public employees: EmployeeI[] = [];
  public loading = true;
  public filterText = "";
  public page = 1;
  public pageSize = 10;
  public closeResult = "";
  public idToEliminate = 0;

  constructor(
    private employeesService: EmployeeService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {

  }

  search() {
    this.loading = true;
    this.employeesService.getListEmployees().subscribe(res => {
      this.employees = res;
      this.loading = false;
    })
  }

  delete() {
    if(this.idToEliminate == 0){
      return;
    }

    this.loading = true;
    this.employeesService.deleteEmployee(this.idToEliminate).subscribe(res => {
      this.modalService.dismissAll();
      this.search();
    })
  }


  edit(employee: EmployeeI) {
    this.router.navigate(['edit', employee]);
  }

  register() {
    this.router.navigate(['register']);
  }

  open(content: any, id: number) {
    this.idToEliminate = id;
		this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
	}
}
