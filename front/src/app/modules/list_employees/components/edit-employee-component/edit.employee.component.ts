import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

interface EmployeeI {
  id: number,
  primerApellido: string,
  segundoApellido: string,
  primerNombre: string,
  otrosNombres: string,
  paisEmpleo: string,
  tipoIdentificacion: string,
  numeroIdentificacion: string,
  correElectronico: string,
  fechaIngreso: string,
  area: string,
  estado: string,
  fechaHoraRegistro: string
}

@Component({
  selector: 'app-edit-employee-component',
  templateUrl: './edit.employee.component.html',
  styleUrls: ['./edit.employee.component.scss']
})
export class EditEmployeecomponent implements OnInit {
  public employees: EmployeeI[] = [];
  public employeeForm: any;

  constructor(
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    console.log(this.route.snapshot.params);

    this.employeeForm = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
    });
  }
}
