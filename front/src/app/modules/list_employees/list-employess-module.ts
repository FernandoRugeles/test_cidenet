import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListEmployessRouterModule } from './list-employess-routing.module';
import { EmployeeService } from 'src/app/services/employee.service';
import { HttpClientModule } from '@angular/common/http';
import { FilterPipe } from 'src/app/pipes/filter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { EditEmployeecomponent } from './components/edit-employee-component/edit.employee.component';
import { ListEmployessComponent } from './components/list-employess-component/list-employess.component';
import { RegisterEmployeeComponent } from './components/register-employee-component/register-employee.component';

@NgModule({
  declarations: [
    ListEmployessComponent,
    FilterPipe,
    EditEmployeecomponent,
    RegisterEmployeeComponent
  ],
  imports: [
    CommonModule,
    ListEmployessRouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbPaginationModule
  ],
  providers: [EmployeeService],
})
export class ListEmployessModule { }
