import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class EmployeeService {

  constructor(private http: HttpClient) {

  }

  getListEmployees() {
    return this.http.get<[]>('http://localhost:8080/api/employee');
  }

  registerEmployee(employee: object) {
    const headers = { 'content-type': 'application/json' }
    return this.http.post<any>('http://localhost:8080/api/employee', employee, { headers }).subscribe();
  }

  deleteEmployee(id: number) {
    return this.http.delete(`http://localhost:8080/api/employee/${id}`);
  }
}
