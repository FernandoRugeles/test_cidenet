export enum AreaEnum {
    Administracion = 'Administración',
    Financiera = 'Financiera',
    Compras = 'Compras',
    Infraestructura = 'Infraestructura',
    Operacion = 'Operación',
    TalentoHumano = 'Talento Humano',
    ServiciosVarios = 'Servicios Varios',
}