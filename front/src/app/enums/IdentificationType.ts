export enum IdentificationTypeEnum {
    CedulaCiudadania = 'Cédula de Ciudadanía',
    CedulaExtranjeria = 'Cédula Extranjería',
    Pasaporte = 'Pasaporte',
    PermisoEspecial = 'Permiso Especial'
}