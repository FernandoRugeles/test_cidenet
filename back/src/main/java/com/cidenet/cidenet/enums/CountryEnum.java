package com.cidenet.cidenet.enums;

public enum CountryEnum {
    COLOMBIA("Colombia"),
    EEUU("Estados Unidos");

    public final String label;

    CountryEnum(String label) {
        this.label = label;
    }
}
