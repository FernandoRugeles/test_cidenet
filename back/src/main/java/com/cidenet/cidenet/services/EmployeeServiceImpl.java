package com.cidenet.cidenet.services;

import com.cidenet.cidenet.dtos.EmailGeneratedDto;
import com.cidenet.cidenet.dtos.EmployeeDto;
import com.cidenet.cidenet.entities.Employee;
import com.cidenet.cidenet.exceptions.EmployeeDuplicatedException;
import com.cidenet.cidenet.repositories.EmployeeRepository;
import com.cidenet.cidenet.use_cases.RegisterEmployeeUseCase;
import com.cidenet.cidenet.utils.FragmentEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    private FragmentEmail fragmentEmail = new FragmentEmail();

    @Override
    public void registerEmployee(EmployeeDto employee) {
        existEmployee(employee.getTipoIdentificacion(), employee.getNumeroIdentificacion());

        RegisterEmployeeUseCase useCase = new RegisterEmployeeUseCase();
        Employee employeeEntity = useCase.register(employee);
        ifExistEmailGenerateNewId(employeeEntity);

        employeeRepository.save(employeeEntity);
    }

    @Override
    public List<Employee> findAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public void deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
    }

    private void existEmployee(String identificationType, String identificationNumber) {
        Employee res = employeeRepository
                .findByTipoIdentificacionAndNumeroIdentificacion(
                        identificationType,
                        identificationNumber
                );

        if (res != null) {
            throw new EmployeeDuplicatedException("there can't be two employees with the same type and identification number");
        }
    }

    private void ifExistEmailGenerateNewId(Employee employee) {
        String ID = "";

        EmailGeneratedDto emailGenerated = new EmailGeneratedDto(
                employee.getPrimerNombre(),
                employee.getPrimerApellido(),
                ID,
                employee.getPaisEmpleo()
        );

        String email = emailGenerated.buildEmail();

        String[] emailSplit = email.split("\\.");

        String emailCut = emailGenerated.buildEmailNotDomain();
        String domain = emailSplit[emailSplit.length-1];

        List<Employee> res = employeeRepository.findByCorreElectronicoLike(emailCut, domain);
        if (res != null && res.size() > 0) {
            emailGenerated.setId(fragmentEmail.splitEmailAdGetId(res.get(res.size() - 1).getCorreElectronico()));
        }

        employee.setCorreElectronico(emailGenerated.buildEmail());
    }
}
