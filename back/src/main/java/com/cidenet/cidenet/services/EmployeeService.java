package com.cidenet.cidenet.services;

import com.cidenet.cidenet.dtos.EmployeeDto;
import com.cidenet.cidenet.entities.Employee;

import java.math.BigInteger;
import java.util.List;

public interface EmployeeService {
    void registerEmployee(EmployeeDto employee);

    List<Employee> findAllEmployees();

    void deleteEmployee(Long id);
}
