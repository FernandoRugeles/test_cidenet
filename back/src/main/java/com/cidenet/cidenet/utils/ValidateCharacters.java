package com.cidenet.cidenet.utils;

public class ValidateCharacters {
    public Boolean validateCharacterAtoZandAccents(String value) {
        boolean canContinue = true;
        if (!isStringOnlyAlphabet(value)) {
            canContinue = false;
        }
        return canContinue;
    }

    public Boolean validateCharactersAlphanumeric(String value) {
        boolean canContinue = true;
        if (!isStringOnlyAlphanumeric(value)) {
            canContinue = false;
        }
        return canContinue;
    }

    private boolean isStringOnlyAlphabet(String str) {
        return ((str != null) && (!str.equals(""))
                && (str.matches("^[A-Z\\s]*$")));
    }

    private boolean isStringOnlyAlphanumeric(String str) {
        return ((str != null) && (!str.equals(""))
                && (str.matches("^[a-zA-Z0-9]+$")));
    }
}
