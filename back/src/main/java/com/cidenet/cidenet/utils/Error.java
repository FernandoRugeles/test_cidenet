package com.cidenet.cidenet.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Error {
    private String mensajeUno;
    private String mensajeDos;
}