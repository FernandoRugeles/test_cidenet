package com.cidenet.cidenet.use_cases;

import com.cidenet.cidenet.dtos.EmployeeDto;
import com.cidenet.cidenet.entities.Employee;
import com.cidenet.cidenet.enums.CountryEnum;
import com.cidenet.cidenet.exceptions.InvalidCharacterException;
import com.cidenet.cidenet.utils.ValidateCharacters;
import org.springframework.beans.BeanUtils;

import java.util.HashMap;
import java.util.Map;

public class RegisterEmployeeUseCase {
    ValidateCharacters validate = new ValidateCharacters();

    public Employee register(EmployeeDto employee) {
        validateAToZAndAccentsCharactersBeforeRegister(employee);
        validateCharactersIdentificationNumberBeforeRegister(employee.getNumeroIdentificacion());
        validateCountry(employee.getPaisEmpleo());

        Employee employeeEntity = new Employee();
        BeanUtils.copyProperties(employee, employeeEntity);
        return employeeEntity;
    }

    private void validateAToZAndAccentsCharactersBeforeRegister(EmployeeDto employee) {
        Map<String, String> mapToValidateAToZ = new HashMap<>();

        mapToValidateAToZ.put("Primer apellido", employee.getPrimerApellido());
        mapToValidateAToZ.put("Segundo Apellido", employee.getSegundoApellido());
        mapToValidateAToZ.put("Primer Nombre", employee.getPrimerNombre());
        mapToValidateAToZ.put("Otros Nombres", employee.getOtrosNombres());

        for (String key : mapToValidateAToZ.keySet()) {
            String value = mapToValidateAToZ.get(key);
            if (value != null) {
                boolean validatedAtoZAndAccentsCharacter = validate.validateCharacterAtoZandAccents(value);
                if (!validatedAtoZAndAccentsCharacter) {
                    throw new InvalidCharacterException("The field ".concat(key).concat(" using the value ").concat(value).concat(" does not follow the rules Only character A to Z and without accents"));
                }
            }
        }
    }

    private void validateCharactersIdentificationNumberBeforeRegister(String identificationNumber) {
        boolean validatedAtoZAndAccentsCharacter = validate.validateCharactersAlphanumeric(identificationNumber);
        if (!validatedAtoZAndAccentsCharacter) {
            throw new InvalidCharacterException("The field Número de identificación ".concat(identificationNumber).concat(" does not follow the rules Only character a-z / A-Z / 0-9 / "));
        }
    }

    private void validateCountry(String country) {
        if(!CountryEnum.EEUU.label.equals(country) && !CountryEnum.COLOMBIA.label.equals(country)){
            throw new InvalidCharacterException("Incorrect country ".concat(country).concat(" must be 'Estados Unidos' or 'Colombia' "));
        }
    }
}
