package com.cidenet.cidenet.entities;

import jakarta.persistence.*;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

@Data
@Entity
@Table(name = "tb_empleado")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_empleado")
    private Long id;

    @Column(name = "primer_apellido")
    @NotNull
    @NotEmpty
    @Size(max = 20)
    private String primerApellido;

    @Column(name = "segundo_apellido")
    @NotNull
    @NotEmpty
    @Size(max = 20)
    private String segundoApellido;

    @Column(name = "primer_nombre")
    @NotNull
    @NotEmpty
    @Size(max = 20)
    private String primerNombre;

    @Column(name = "otros_nombres")
    @NotNull
    @NotEmpty
    @Size(max = 50)
    private String otrosNombres;

    @Column(name = "pais_empleo")
    private String paisEmpleo;

    @Column(name = "tipo_identificacion")
    @NotNull
    @NotEmpty
    private String tipoIdentificacion;

    @Column(name = "numero_identificacion")
    @NotNull
    @NotEmpty
    @Size(max = 20)
    private String numeroIdentificacion;

    @Column(name = "corre_electronico")
    @Email
    @Size(max = 300)
    private String correElectronico;

    @Column(name = "fecha_ingreso")
    private Date fechaIngreso;

    @Column(name = "area")
    private String area;

    @Column(name = "estado")
    private Integer estado;

    @Column(name = "fecha_hora_registro")
    private Timestamp fechaHoraRegistro;
}
