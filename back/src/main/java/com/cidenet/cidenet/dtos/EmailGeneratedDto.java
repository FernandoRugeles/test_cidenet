package com.cidenet.cidenet.dtos;

import com.cidenet.cidenet.enums.CountryEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.Arrays;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
public class EmailGeneratedDto {
    private String firstName;
    private String firstSurname;
    private String id;
    private String country;

    private final String countryColombia = "cidenet.com.co";
    private final String countryEstadosUnidos = "cidenet.com.us";

    public String buildEmail() {
        String domain = "";

        if (CountryEnum.COLOMBIA.label.equalsIgnoreCase(this.country)) {
            domain = countryColombia;
        } else if (CountryEnum.EEUU.label.equalsIgnoreCase(this.country)) {
            domain = countryEstadosUnidos;
        }

        StringBuilder email = new StringBuilder();
        email.append(buildEmailNotDomain());

        if (this.getId() != null && !this.getId().isEmpty()) {
            email.append(".").append(this.id);
        }

        email.append("@").append(domain);
        return email.toString().toLowerCase();
    }

    public String buildEmailNotDomain() {
        String firstNameSplit[] = this.getFirstName().trim().split(" ");
        String firstSurnameSplit[] = this.getFirstSurname().trim().split(" ");

        if (firstNameSplit.length > 1) {
            this.firstName = firstNameSplit[0];
        }

        if (firstSurnameSplit.length > 1) {
            this.firstSurname = Arrays.stream(firstSurnameSplit).collect(Collectors.joining());
        }

        StringBuilder email = new StringBuilder();
        email.append(this.firstName).append(".").append(this.firstSurname);

        return email.toString().toLowerCase();
    }
}
