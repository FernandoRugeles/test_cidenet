package com.cidenet.cidenet.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Date;

@Data
@AllArgsConstructor
public class EmployeeDto {
    private String primerApellido;

    private String segundoApellido;

    private String primerNombre;

    private String otrosNombres;

    private String paisEmpleo;

    private String tipoIdentificacion;

    private String numeroIdentificacion;

    private Date fechaIngreso;

    private String area;

    private Integer estado;

    private Timestamp fechaHoraRegistro;
}
