package com.cidenet.cidenet.repositories;

import com.cidenet.cidenet.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Employee findByTipoIdentificacionAndNumeroIdentificacion(String identificationNumber, String identificationType);

    @Query("SELECT a FROM Employee a where a.correElectronico like :email% and a.correElectronico like %:domain")
    List<Employee> findByCorreElectronicoLike(@Param("email") String email, @Param("domain") String domain);
}
