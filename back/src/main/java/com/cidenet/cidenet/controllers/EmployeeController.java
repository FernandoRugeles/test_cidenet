package com.cidenet.cidenet.controllers;


import com.cidenet.cidenet.dtos.EmployeeDto;
import com.cidenet.cidenet.entities.Employee;
import com.cidenet.cidenet.services.EmployeeServiceImpl;
import com.cidenet.cidenet.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import java.util.List;

@Validated
@RequestMapping("/api/employee")
@RestController
public class EmployeeController {

    @Autowired
    private EmployeeServiceImpl employeeService;
    private final Response util = new Response();

    @PostMapping
    public ResponseEntity registerEmployee(@RequestBody EmployeeDto employee) {
        try {
            employeeService.registerEmployee(employee);
            return ResponseEntity.status(HttpStatus.OK).body(employee);
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @GetMapping
    public ResponseEntity findAllEmployees() {
        try {
            List<Employee> res = employeeService.findAllEmployees();
            return ResponseEntity.status(HttpStatus.OK).body(res);
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity deleteEmployee(@PathVariable Long id) {
        try {
            employeeService.deleteEmployee(id);
            return new ResponseEntity<>(id, HttpStatus.OK);
        } catch (ConstraintViolationException ex) {
            return util.responseViolationException(ex, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return util.responseException(e, HttpStatus.INTERNAL_SERVER_ERROR, false);
        }
    }
}
