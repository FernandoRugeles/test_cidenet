package com.cidenet.cidenet.exceptions;

public class EmployeeDuplicatedException extends RuntimeException {
    public EmployeeDuplicatedException(String message) {
        super(message);
    }
}
